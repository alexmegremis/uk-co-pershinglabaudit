'use strict';

angular.module('gitLabAuditApplication', [
        'ngRoute',
        'gitLabAuditApplication.branchServices',
        'gitLabAuditApplication.branchControllers'
        //'bookingsManagementApp.bookingsControllers',
        //'bookingsManagementApp.bookingsDirectives',
        //'ngAnimate',
        //'ngSanitize',
        //'mgcrea.ngStrap'
    ])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when("/branches/:branchID", {
            templateUrl: "views/branches.html",
            controller: "branchesController"
        });
    }]);
