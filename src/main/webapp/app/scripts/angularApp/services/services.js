'use strict';

angular.module('gitLabAuditApplication.branchServices', [])
    .factory('branchAPIService', function ($http) {
        var branchAPIService = {};

        branchAPIService.getAll = function (projectID) {
            return $http.get('/api/v1/projects/' + projectID + '/branches');
        };
        return branchAPIService;
    });
//
//angular.module('bookingsManagementApp.bookingsServices', [])
//
//    .factory('timepickerState', function() {
//        var pickers = [];
//        return {
//            addPicker: function(picker) {
//                pickers.push(picker);
//            },
//            closeAll: function() {
//                for (var i=0; i<pickers.length; i++) {
//                    pickers[i].close();
//                }
//            }
//        };
//    })
//
//    .factory('messageService', function($rootScope, $log) {
//
//        var messageService = {};
//
//        messageService.updatedBooking = function(booking) {
//            $rootScope.$broadcast("updatedBooking", booking);
//            $log.info(">>> Broadcasted!");
//        };
//
//        return messageService;
//    })
//
//    .factory('bookingsAPIService', function ($http, $log, messageService) {
//
//        var bookingsAPIService = {};
//
//        bookingsAPIService.getAll = function() {
//            return $http.get('/bookings/api/v1/bookings/1')
//        };
//
//        bookingsAPIService.get = function(id) {
//            return $http.get('/bookings/api/v1/bookings/1/' + id);
//        };
//
//        bookingsAPIService.save = function(booking) {
//            return $http.post('/bookings/api/v1/bookings/1', booking);
//        };
//
//        bookingsAPIService.update = function(booking) {
//            return $http.put('/bookings/api/v1/bookings/1', booking);
//        };
//
//        bookingsAPIService.delete = function(booking) {
//            return $http.delete('/bookings/api/v1/bookings/1', booking);
//        };
//
//        bookingsAPIService.updateAndBroadcast = function(booking) {
//
//            var request = {"BookingEntity" : booking};
//
//            bookingsAPIService.save(request).success(function(response) {
//                $log.info(">>> booking updated successfully");
//                messageService.updatedBooking(response);
//            });
//        };
//
//        return bookingsAPIService;
//    })
//
//    .factory('locationsAPIService', function ($http) {
//
//        var locationsAPIService = {};
//
//        locationsAPIService.getAll = function() {
//            return $http.get('/bookings/api/v1/locations/1');
//        };
//
//        return locationsAPIService;
//
//    })
//
//    .factory('vehiclesAPIService', function ($http) {
//
//        var vehiclesAPIService = {};
//
//        vehiclesAPIService.getAll = function() {
//            return $http.get('/bookings/api/v1/vehicles/1');
//        };
//
//        return vehiclesAPIService;
//
//    })
//
//    .factory('configAPIService', function ($http) {
//
//        var configAPIService = {};
//
//        configAPIService.getBookingStatuses = function() {
//            return $http.get('/bookings/api/v1/config/bookingStatus');
//        };
//
//        return configAPIService;
//    });
