'use strict';

angular.module('gitLabAuditApplication.branchControllers', [])
    .controller('branchesController', function ($rootScope, $scope, $routeParams, $route, $window, $log, branchAPIService) {

        $scope.branches = [];

        branchAPIService.getAll($routeParams.branchID).success(function (response) {
            $log.info(response);
            $scope.branches = response;
        });

    });

//angular.module('bookingsManagementApp.bookingsControllers', []).
//
//    controller('bookingsListController', function ($scope, $window, $log, $modal,
//                                                   bookingsAPIService,
//                                                   configAPIService) {
//
//        $scope.currentDate = new Date();
//        $scope.bookings = [];
//
//        $scope.config = {};
//        $scope.config.statuses = [];
//
//        $scope.colSort = {};
//        $scope.colSort.predicate = 'datetime';
//        $scope.colSort.reverse = false;
//
//        $scope.order='datetime';
//        $scope.reverse=false;
//
//        $scope.setHigherStatus = function(booking) {
//            $log.info(">>> setHigherStatus");
//            if(booking.status.idNext != booking.status.id) {
//                setStatus(booking, booking.status.idNext);
//            }
//            $scope.updateBooking(booking);
//        };
//
//        $scope.setLowerStatus = function(booking) {
//            $log.info(">>> setLowerStatus");
//            if(booking.status.idPrevious != booking.status.id) {
//                setStatus(booking, booking.status.idPrevious);
//            }
//            $scope.updateBooking(booking);
//        };
//
//        var setStatus = function(booking, newStatusId) {
//            $scope.config.statuses.forEach(function(aStatus) {
//                var foundNext = false;
//                if(!foundNext && aStatus.id == newStatusId) {
//                    booking.status = angular.copy(aStatus);
//                    foundNext = true;
//                }
//            });
//        };
//
//        var editModal = $modal({scope: $scope, template: 'views/bookingDetails.html', show: false});
//        $scope.editBooking = function (booking)  {
//            //$modal.$scope.booking = booking;
//            editModal.$promise.then(editModal.show);
//        };
//
//        $scope.editBookingOld = function (booking) {
//
//            var modalInstance = $modal.open({
//                templateUrl: 'views/bookingDetails.html',
//                controller: 'bookingDetailController',
//                size: 'med',
//                resolve: {
//                    bookingObject: function () {
//                        return booking;
//                    }
//                }
//            });
//        };
//
//        bookingsAPIService.getAll().success(function(response) {
//
//            response.forEach(function(aBooking) {
//                if(typeof aBooking.datetime != "undefined") {
//                    aBooking.datetime = new Date(aBooking.datetime);
//                }
//            });
//
//            $scope.bookings = response;
//            $log.info($scope.bookings);
//        });
//
//        configAPIService.getBookingStatuses().success(function(response) {
//            $log.info(response);
//            $scope.config.statuses = response;
//
//            $scope.statusDropdown = [];
//
//            $scope.config.statuses.forEach(function(aStatus) {
//                $scope.statusDropdown.push(
//                    {
//                        "text"  : "<div class=\"STATUS_" + aStatus.name + "\">" + aStatus.nameDisplay + "</div>",
//                        'click' : "{aBooking.status = aStatus}"
//                    }
//                );
//            });
//        });
//
//        $scope.$on("updatedBooking", function(event, updatedBooking) {
//            $log.info(">>> Broadcast received");
//            bookingsAPIService.get(updatedBooking[0].id).success(function(response) {
//
//                var retrievedBooking = response[0];
//                $log.info(">>> Retrieved updated booking from DB");
//                $log.info(response);
//
//                for(var i = 0; i < $scope.bookings.length; i++) {
//                    if($scope.bookings[i].id == retrievedBooking.id) {
//                        if(typeof retrievedBooking.datetime != "undefined") {
//                            retrievedBooking.datetime = new Date(retrievedBooking.datetime);
//                        }
//                        $scope.bookings[i] = retrievedBooking;
//                        break;
//                    }
//                }
//            });
//        });
//
//        $scope.updateBooking = function(booking) { bookingsAPIService.updateAndBroadcast(booking); };
//
//    })
//
//    .controller('bookingDetailController', function($scope, $log, $modalInstance, bookingObject,
//                                                          timepickerState,
//                                                          messageService,
//                                                          bookingsAPIService,
//                                                          locationsAPIService,
//                                                          vehiclesAPIService,
//                                                          configAPIService) {
//
//        $scope.booking = angular.copy(bookingObject);
//
//        /**
//         * buttons
//         */
//        $scope.ok = function () {
//            $scope.datetimeOpened = false;
//            $modalInstance.close();
//        };
//        $scope.cancel = function () {
//            $scope.datetimeOpened = false;
//            $modalInstance.dismiss('cancel');
//        };
//
//        $scope.isCalendarOpened = false;
//        $scope.open = function($event) {
//            $event.preventDefault();
//            $event.stopPropagation();
//
//            $scope.isCalendarOpened = true;
//        };
//
//        $scope.dateOptions = {
//            formatYear: 'yy',
//            startingDay: 1
//        };
//        $scope.formats = ['dd-MMMM-yyyy HH:mm', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
//        $scope.format = $scope.formats[0];
//
//
//        $scope.openCalendar = function($event) {
//            $event.preventDefault();
//            $event.stopPropagation();
//
//            $scope.datetimeOpened = true;
//        };
//
//        $scope.updateBooking = function() { bookingsAPIService.updateAndBroadcast($scope.booking); };
//
//        $scope.locations = {};
//
//        locationsAPIService.getAll().success(function(response) {
//            $scope.locations = response;
//        });
//
//        $scope.vehicles = {};
//
//        vehiclesAPIService.getAll().success(function(response) {
//            $scope.vehicles = response;
//        });
//
//        configAPIService.getBookingStatuses().success(function(response) {
//            $scope.statuses = response;
//        });
//    });
