'use strict';

angular.module('bookingsManagementApp.bookingsDirectives', [])

    .directive("sortable", function($log) {
        return {
            restrict    : 'A',
            transclude  : true,
            replace     : true,
            template    :
                '<th ng-click="onClick()" class="{{class}}">' +
                    '<div class="pull-left" ng-transclude></div>' +
                    '<div class="pull-right" ng-class="{\'inactive\' : order !== by}"><div class="orderIcon glyphicon" ng-class="{\'glyphicon-sort-by-alphabet\' : !reverse,  \'glyphicon-sort-by-alphabet-alt\' : reverse}"></div></div>' +
                '</th>',
            scope: {
                order   : '=',
                by      : '=',
                reverse : '='
            },
            link: function(scope) {
                scope.onClick = function () {
                    if( scope.order === scope.by ) {
                        scope.reverse = !scope.reverse
                    } else {
                        scope.by = scope.order ;
                        //scope.reverse = false;
                    }
                    $log.info(">> CLICK " + scope.order + " + " + scope.by + " + " + scope.reverse);
                };
            }
        }
    })

    .directive("remembers", function($log) {
        return {
            require     : "ngModel",
            restrict    : "A",
            link: function(scope, elem, attrs, ctrl) {

                elem.bind('focus', function () {
                    scope.oldValue = ctrl.$modelValue;
                    scope.$eval(attrs.ngModel + "=" + null);

                    ctrl.$render();
                    scope.$apply();
                });
                elem.bind('blur', function () {
                    if(scope.$modelValue == null && scope.oldValue != null) {
                        scope.$eval(attrs.ngModel + "=" + scope.oldValue);

                        ctrl.$render();
                        scope.$apply();
                    }
                });
            }
        }
    });
