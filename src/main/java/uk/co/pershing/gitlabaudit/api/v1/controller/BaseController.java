package uk.co.pershing.gitlabaudit.api.v1.controller;

import org.apache.cxf.jaxrs.impl.ResponseBuilderImpl;
import uk.co.pershing.gitlabaudit.dto.out.OutBaseDTO;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 21/10/14.
 */
public abstract class BaseController<T extends OutBaseDTO> {

    protected static final Integer RESPONSE_STATUS_OK = Response.Status.OK.getStatusCode();

    protected Response getResponse(final List<T> payload, final Integer statusCode) {
        Response response = new ResponseBuilderImpl().
                entity(payload).
                status(Response.Status.fromStatusCode(statusCode)).
                build();
        return response;
    }

    protected Response getResponse(final T payload, final Integer statusCode) {
        List<T> payloadInList = new ArrayList<>();
        payloadInList.add(payload);
        return getResponse(payloadInList, statusCode);
    }

}
