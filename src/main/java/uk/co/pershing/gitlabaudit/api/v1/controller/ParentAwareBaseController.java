package uk.co.pershing.gitlabaudit.api.v1.controller;

import org.apache.cxf.jaxrs.impl.ResponseBuilderImpl;
import uk.co.pershing.gitlabaudit.dto.out.OutBaseDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 21/10/14.
 */
public abstract class ParentAwareBaseController<T extends OutBaseDTO> {

    protected static final Integer RESPONSE_STATUS_OK = Response.Status.OK.getStatusCode();

    protected Response getResponse(final List<T> payload, final Integer statusCode) {
        Response response = new ResponseBuilderImpl().
                entity(payload).
                status(Response.Status.fromStatusCode(statusCode)).
                build();
        return response;
    }

    protected Response getResponse(final T payload, final Integer statusCode) {
        List<T> payloadInList = new ArrayList<>();
        payloadInList.add(payload);
        return getResponse(payloadInList, statusCode);
    }

    protected abstract ParentAwareBaseProvider<T> getProvider();

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response getSingle(@PathParam("parentID") final String parentID,
                              @PathParam("id") final String id) {

        T result = doGetSingle(parentID, id);
        Response response = getResponse(result, RESPONSE_STATUS_OK);

        return response;
    }

    protected T doGetSingle(final String parentID, final String id) {
        T result = getProvider().getSingle(parentID, id);
        return result;
    }

    @GET
    @Path("/")
    @Produces("application/json")
    public Response getAll(@PathParam("parentID") final String parentID) {

        List<T> result = getProvider().getAll(parentID);
        Response response = getResponse(result, RESPONSE_STATUS_OK);

        return response;
    }

    // @TODO : Do we need this?
//    protected List<T> doGetAll(final String projectID) {
//        List<T> result = doGetAll(projectID);
//        return result;
//    }

}
