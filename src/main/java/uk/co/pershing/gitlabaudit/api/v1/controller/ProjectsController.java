package uk.co.pershing.gitlabaudit.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import uk.co.pershing.gitlabaudit.dto.out.OutProjectDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
//@WebService
@Path("/projects")
public class ProjectsController extends BaseController<OutProjectDTO> {

    @Autowired
    private BaseProvider<OutProjectDTO> outProjectProvider;

    @GET
    @Produces("application/json")
    public Response getAllProjects() {

        List<OutProjectDTO> projects = outProjectProvider.getAll();
        Response response = getResponse(projects, RESPONSE_STATUS_OK);

        return response;
    }

    @GET
    @Path("/{projectID}")
    @Produces("application/json")
    public Response getProject(@PathParam("projectID") final String projectID) {

        OutProjectDTO project = outProjectProvider.getSingle(projectID);
        Response response = getResponse(project, RESPONSE_STATUS_OK);

        return response;
    }
}
