package uk.co.pershing.gitlabaudit.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import uk.co.pershing.gitlabaudit.dto.out.OutBranchDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by alexmegremis on 29/09/15.
 */
//@WebService
@Path("/projects/{parentID}/branches")
public class BranchesController extends ParentAwareBaseController<OutBranchDTO> {

    @Autowired
    private ParentAwareBaseProvider<OutBranchDTO> outBranchProvider;

    @Autowired
    private ParentAwareBaseProvider<OutBranchDTO> outBranchHistoryProvider;

    @Override
    protected ParentAwareBaseProvider<OutBranchDTO> getProvider() {
        return outBranchProvider;
    }

    @GET
    @Path("{id}/history")
    @Produces("application/json")
    public Response getHistory(@PathParam("parentID") final String parentID,
                               @PathParam("id") final String id) {

        OutBranchDTO branch = outBranchHistoryProvider.getSingle(parentID, id);
        Response result = getResponse(branch, RESPONSE_STATUS_OK);
        return result;
    }
}
