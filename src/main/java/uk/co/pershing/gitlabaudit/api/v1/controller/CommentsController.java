package uk.co.pershing.gitlabaudit.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import javax.ws.rs.Path;

/**
 * Created by alexmegremis on 29/09/15.
 */
//@WebService
@Path("/issues/{parentID}/comments")
public class CommentsController extends ParentAwareBaseController<OutCommitDTO> {

    @Autowired
    private ParentAwareBaseProvider<OutCommitDTO> outCommitProvider;

    @Override
    protected ParentAwareBaseProvider<OutCommitDTO> getProvider() {
        return outCommitProvider;
    }

//    @Override
//    protected List<CommitDTO> doGetAll(final String projectID) {
//        List<CommitDTO> result = new ArrayList<>();
//        return result;
//    }
}
