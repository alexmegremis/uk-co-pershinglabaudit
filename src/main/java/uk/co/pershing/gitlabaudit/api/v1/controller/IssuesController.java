package uk.co.pershing.gitlabaudit.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import uk.co.pershing.gitlabaudit.dto.out.OutIssueDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by alexmegremis on 29/09/15.
 */
//@WebService
@Path("/issues")
public class IssuesController extends BaseController<OutIssueDTO> {

    @Autowired
    private BaseProvider<OutIssueDTO> outIssueProvider;

//    @GET
//    @Produces("application/json")
//    public Response getAllProjects() {
//
//        List<ProjectDTO> projects = uiProjectProvider.getAll();
//        Response response = getResponse(projects, RESPONSE_STATUS_OK);
//
//        return response;
//    }

    @GET
    @Path("/{issueID}")
    @Produces("application/json")
    public Response getIssue(@PathParam("issueID") final String issueID) {

        OutIssueDTO result = outIssueProvider.getSingle(issueID);
        Response response = getResponse(result, RESPONSE_STATUS_OK);

        return response;
    }
}
