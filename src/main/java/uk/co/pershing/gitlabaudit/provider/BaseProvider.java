package uk.co.pershing.gitlabaudit.provider;

import uk.co.pershing.gitlabaudit.dto.BaseDTO;

import java.util.List;

/**
 * This is the basic signature for all data providers within the API.
 * <p/>
 * Created by alexmegremis on 29/09/15.
 */
public interface BaseProvider<T extends BaseDTO> {

    /**
     * Retrieve a single item, based on ID.
     *
     * @param id
     * @return
     */
    T getSingle(final String id);

    /**
     * Retrieve all items of this type from the source.
     *
     * @return
     */
    List<T> getAll();
}
