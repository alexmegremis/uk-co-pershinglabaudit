package uk.co.pershing.gitlabaudit.provider.in;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;
import uk.co.pershing.gitlabaudit.util.ParentAwareURIFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementations have a GL API URL path dependency on a parent project ID.
 * For example:
 * GET /projects/:id/repository/branches
 * <p/>
 * Created by alexmegremis on 01/10/2015.
 */
public abstract class BaseProviderImpl<T extends BaseDTO> implements ParentAwareBaseProvider<T>, BaseProvider<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseProviderImpl.class);

    //    @Autowired
    protected abstract ParentAwareURIFactory getParentAwareURIFactory();

    protected RestTemplate getTemplate() {
        return new RestTemplate();
    }

    /**
     * Determine the GL API path for this provider's method.
     *
     * @return
     */
    protected abstract String getMethodPath();

    /**
     * To accomodate convenience methods we need to know the DTO type of the provider that's extending us.
     * (This information is otherwise lost at runtime due to type erasure).
     *
     * @return
     */
    protected abstract Class<T> getType();

    public T getSingle(final String id) {
        return doGetsingle(null, id);
    }

    @Override
    public List<T> getAll() {
        throw new NotImplementedException(">>>> Default getAll() was called. You should override this! Path: ", getMethodPath());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getSingle(final String parentId, final String id) {
        return doGetsingle(parentId, id);
    }

    @Override
    public List<T> getAll(final String projectId) {
        throw new NotImplementedException(">>>> Default getAll(Project) was called. You should override this! Path: ", getMethodPath());
    }

    private T doGetsingle(final String parentId, final String id) {
        String url = getParentAwareURIFactory().getUrl(parentId, getMethodPath(), id);
        T result = getTemplate().getForObject(url, getType());
        return result;
    }

    /**
     * Retrieve a group of items, based on individual IDs.
     *
     * @param ids
     * @return
     */
    public List<T> getGroup(final String parentId, final String... ids) {
        return doGetGroup(parentId, ids);
    }

    private List<T> doGetGroup(final String parentId, final String... ids) {

        RestTemplate template = getTemplate();
        List<T> result = new ArrayList<>();
        String url = null;

        for (String id : ids) {
            url = getParentAwareURIFactory().getUrl(parentId, getMethodPath(), id);
            T aDTO = template.getForObject(url, getType());
            result.add(aDTO);
        }

        return result;
    }
}
