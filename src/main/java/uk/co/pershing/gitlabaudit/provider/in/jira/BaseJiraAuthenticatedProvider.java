package uk.co.pershing.gitlabaudit.provider.in.jira;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;
import uk.co.pershing.gitlabaudit.provider.in.BaseProviderImpl;
import uk.co.pershing.gitlabaudit.provider.interceptor.HeaderRequestInterceptor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 12/11/2015.
 */
public abstract class BaseJiraAuthenticatedProvider<T extends BaseDTO> extends BaseProviderImpl<T> {

    public static final String HEADER_AUTH_NAME = "Authorization";
    public static final String HEADER_AUTH_VALUE_PREFIX = "Basic ";
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseJiraAuthenticatedProvider.class);
    @Value("${jira.auth.string}")
    private String headerAuthValue;

    protected RestTemplate getTemplate() {
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

        interceptors.add(new HeaderRequestInterceptor(HEADER_AUTH_NAME, HEADER_AUTH_VALUE_PREFIX + headerAuthValue));

        RestTemplate result = new RestTemplate();
        result.setInterceptors(interceptors);

        return result;
    }
}
