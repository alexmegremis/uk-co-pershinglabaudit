package uk.co.pershing.gitlabaudit.provider.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutIssueDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.EnrichmentProvider;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("outIssueProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class IssueProviderImpl extends BaseExternalDataProviderImpl<OutIssueDTO, JiraIssueDTO> {// implements BaseProvider<UIIssueDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IssueProviderImpl.class);

    @Autowired
    private EnrichmentProvider<OutIssueDTO> issueEnrichmentProvider;

    @Autowired
    private BaseProvider<JiraIssueDTO> jiraIssueProvider;

    @Override
    protected Class<OutIssueDTO> getType() {
        return OutIssueDTO.class;
    }

    @Override
    protected uk.co.pershing.gitlabaudit.provider.BaseProvider getExternalProvider() {
        return jiraIssueProvider;
    }

    @Override
    public EnrichmentProvider<OutIssueDTO>[] getEnrichmentProviders() {
        return new EnrichmentProvider[]{issueEnrichmentProvider};
    }

}
