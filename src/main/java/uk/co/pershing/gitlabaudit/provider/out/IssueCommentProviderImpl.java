package uk.co.pershing.gitlabaudit.provider.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueCommentDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutIssueCommentDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.ParentAwareEnrichmentProvider;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("outIssueCommentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class IssueCommentProviderImpl extends BaseExternalDataProviderImpl<OutIssueCommentDTO, JiraIssueCommentDTO> {// implements BaseProvider<OutIssueDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IssueCommentProviderImpl.class);

    @Autowired
    private ParentAwareBaseProvider<JiraIssueCommentDTO> jiraIssueCommentProvider;

    @Override
    protected Class<OutIssueCommentDTO> getType() {
        return OutIssueCommentDTO.class;
    }

    @Override
    protected ParentAwareBaseProvider<JiraIssueCommentDTO> getExternalParentAwareProvider() {
        return jiraIssueCommentProvider;
    }

    @Override
    public ParentAwareEnrichmentProvider<OutIssueCommentDTO>[] getParentAwareEnrichmentProviders() {
        return null;
    }

}
