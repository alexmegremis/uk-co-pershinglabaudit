package uk.co.pershing.gitlabaudit.provider.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLCommitDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.ParentAwareEnrichmentProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("outCommitProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CommitProviderImpl extends BaseExternalDataProviderImpl<OutCommitDTO, GLCommitDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommitProviderImpl.class);

    @Autowired
    private ParentAwareEnrichmentProvider<OutCommitDTO> commitEnrichmentProvider;
    @Autowired
    private ParentAwareEnrichmentProvider<OutCommitDTO> commitIssueEnrichmentProvider;
    @Autowired
    private ParentAwareBaseProvider<GLCommitDTO> glCommitProvider;

    @Override
    protected Class<OutCommitDTO> getType() {
        return OutCommitDTO.class;
    }

    @Override
    protected ParentAwareBaseProvider<GLCommitDTO> getExternalParentAwareProvider() {
        return glCommitProvider;
    }

    @Override
    public ParentAwareEnrichmentProvider<OutCommitDTO>[] getParentAwareEnrichmentProviders() {
        return new ParentAwareEnrichmentProvider[]{
                commitEnrichmentProvider,
                commitIssueEnrichmentProvider
        };
    }

    @Override
    public List<OutCommitDTO> doGetAll(final String projectID) {

        glCommitProvider.getAll(projectID);

        List<GLCommitDTO> placeholder = glCommitProvider.getAll(projectID);
        List<OutCommitDTO> result = new ArrayList<>();

        for (GLCommitDTO aPlaceholderCommit : placeholder) {
//            url = getParentAwareURIFactory().getUrl(projectId, getMethodPath(), aPlaceholderCommit.getId());
//            GLCommitDTO aCommit = getTemplate().getForObject(url, GLCommitDTO.class);

            GLCommitDTO aGLCommit = glCommitProvider.getSingle(projectID, aPlaceholderCommit.getId());
            OutCommitDTO aCommit = new OutCommitDTO();
            aCommit.init(aGLCommit);
            result.add(aCommit);
        }

        return result;
    }

}
