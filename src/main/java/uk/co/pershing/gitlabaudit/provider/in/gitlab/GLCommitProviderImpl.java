package uk.co.pershing.gitlabaudit.provider.in.gitlab;

import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLCommitDTO;
import uk.co.pershing.gitlabaudit.provider.in.BaseProviderImpl;
import uk.co.pershing.gitlabaudit.util.ParentAwareURIFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("glCommitProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class GLCommitProviderImpl extends BaseProviderImpl<GLCommitDTO> {

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.path.service.commits}")
    private String methodPath;

    @Autowired
    private ParentAwareURIFactory gitLabURIFactory;

    @Override
    protected ParentAwareURIFactory getParentAwareURIFactory() {
        return gitLabURIFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<GLCommitDTO> getType() {
        return GLCommitDTO.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final List<GLCommitDTO> getAll(final String projectId) {

        String url = getParentAwareURIFactory().getUrl(projectId, getMethodPath(), null);

        GLCommitDTO[] placeholder = getTemplate().getForObject(url, GLCommitDTO[].class);

        List<GLCommitDTO> result = Arrays.asList(placeholder);
//        List<GLCommitDTO> result = new ArrayList<>();
//
//        for (GLCommitDTO aPlaceholderCommit : placeholder) {
//            GLCommitDTO aCommit = getSingle(projectId, aPlaceholderCommit.getId());
//            result.add(aCommit);
//        }

        return result;
    }
}
