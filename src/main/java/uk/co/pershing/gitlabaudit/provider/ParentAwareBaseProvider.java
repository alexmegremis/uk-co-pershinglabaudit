package uk.co.pershing.gitlabaudit.provider;

import java.util.List;

/**
 * Created by alexmegremis on 01/10/2015.
 */
public interface ParentAwareBaseProvider<T> {

    /**
     * Retrieve a single item, based on ID.
     *
     * @param parentId The ID of the parent entity
     * @param id       The ID of the item being retrieved
     * @return
     */
    T getSingle(final String parentId, final String id);

    /**
     * Retrieve all items of this type from the source.
     *
     * @param parentId The ID of the parent entity
     * @return The retrieved items
     */
    List<T> getAll(final String parentId);

}
