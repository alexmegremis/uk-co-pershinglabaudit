package uk.co.pershing.gitlabaudit.provider.out;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutBaseDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.EnrichmentProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.ParentAwareEnrichmentProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by alexmegremis on 01/10/2015.
 */
public abstract class BaseExternalDataProviderImpl<T extends OutBaseDTO, Y extends BaseDTO> implements BaseProvider, ParentAwareBaseProvider<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseExternalDataProviderImpl.class);

    protected abstract Class<T> getType();

    protected BaseProvider<Y> getExternalProvider() {
        throw new NotImplementedException(">>>> Default getExternalProvider() was called. You should override this!");
    }

    protected ParentAwareBaseProvider<Y> getExternalParentAwareProvider() {
        throw new NotImplementedException(">>>> Default getExternalParentAwareProvider() was called. You should override this!");
    }

    protected EnrichmentProvider<T>[] getEnrichmentProviders() {
        throw new NotImplementedException(">>>> Default getEnrichmentProviders() was called. You should override this!");
    }

    protected ParentAwareEnrichmentProvider<T>[] getParentAwareEnrichmentProviders() {
        throw new NotImplementedException(">>>> Default getParentAwareEnrichmentProviders() was called. You should override this!");
    }

    /**
     * Placeholder for custom implementations.
     *
     * @param projectID
     * @return
     */
    protected List<T> doGetAll(final String projectID) {
        // this will always get handled in parent class.
        throw new NotImplementedException(">>>> Placeholder doGetAll() was called.");
    }

    /**
     * {@inheritDoc}
     *
     * @param id
     * @return
     */
    @Override
    public T getSingle(final String id) {

        Y glResult = getExternalProvider().getSingle(id);
        T result = doGetSingle(glResult);

        doEnrichment(result);

        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    public final List<T> getAll() {
        List<Y> glResult = getExternalProvider().getAll();
        List<T> result = doGetAll(glResult);

        doEnrichment(result);

        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @param projectId
     * @param id        The ID of the item being retrieved
     * @return
     */
    @Override
    public final T getSingle(final String projectId, final String id) {

        Y glResult = getExternalParentAwareProvider().getSingle(projectId, id);
        T result = doGetSingle(glResult);

        doParentAwareEnrichment(result, projectId);

        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @param projectID
     * @return
     */
    public final List<T> getAll(final String projectID) {
        List<T> result = null;

        try {
            result = doGetAll(projectID);
        } catch (NotImplementedException e) {
            List<Y> externalDataResult = getExternalParentAwareProvider().getAll(projectID);
            result = doGetAll(externalDataResult);
        }

        doParentAwareEnrichment(result, projectID);

        return result;
    }

    private T doGetSingle(final Y glDTO) {
        T result = null;
        try {
            result = getType().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        result.init(glDTO);

        return result;
    }

    private List<T> doGetAll(final List<Y> glDtoList) {
        List<T> result = new ArrayList<>();
        for (Y aGlDTO : glDtoList) {
            T aDTO = null;
            try {
                aDTO = getType().newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            aDTO.init(aGlDTO);
            result.add(aDTO);
        }

        return result;
    }

    // @TODO - This is HORRIBLE juju, we're duplicating overrides between this and enrichment providers. FIX!
    private void doEnrichment(final T dto) {
        doEnrichment(Arrays.asList(dto));
    }

    private void doEnrichment(final List<T> dtoList) {
        EnrichmentProvider<T>[] enrichmentProviders = getEnrichmentProviders();
        if (ArrayUtils.isNotEmpty(enrichmentProviders)) {
            for (EnrichmentProvider<T> enrichmentProvider : enrichmentProviders) {
                enrichmentProvider.enrich(dtoList);
            }
        }
    }

    private void doParentAwareEnrichment(final T dto, final String projectID) {
        doParentAwareEnrichment(Arrays.asList(dto), projectID);
    }

    private void doParentAwareEnrichment(final List<T> dtoList, final String projectID) {
        ParentAwareEnrichmentProvider<T>[] parentAwareEnrichmentProviders = getParentAwareEnrichmentProviders();
        if (ArrayUtils.isNotEmpty(parentAwareEnrichmentProviders)) {
            for (ParentAwareEnrichmentProvider<T> enrichmentProvider : parentAwareEnrichmentProviders) {
                enrichmentProvider.enrich(dtoList, projectID);
            }
        }
    }
}
