package uk.co.pershing.gitlabaudit.provider.in.jira;

import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueCommentDTO;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueCommentsContainerDTO;
import uk.co.pershing.gitlabaudit.util.ParentAwareURIFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 13/10/2015.
 */
@Component("jiraIssueValidationProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JiraIssueCommentProviderImpl extends BaseJiraAuthenticatedProvider<JiraIssueCommentDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JiraIssueCommentProviderImpl.class);
    @Autowired
    protected ParentAwareURIFactory jiraURIFactory;
    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.path.service.comments}")
    private String methodPath;

    @Override
    protected ParentAwareURIFactory getParentAwareURIFactory() {
        return jiraURIFactory;
    }

    @Override
    protected Class<JiraIssueCommentDTO> getType() {
        return JiraIssueCommentDTO.class;
    }

    @Override
    public List<JiraIssueCommentDTO> getAll(final String issueId) {
        String url = jiraURIFactory.getUrl(issueId, methodPath, null);

        List<JiraIssueCommentDTO> result = null;

        try {
            JiraIssueCommentsContainerDTO commentsContainer = getTemplate().getForEntity(url, JiraIssueCommentsContainerDTO.class).getBody();
            result = new ArrayList<>(commentsContainer.getComments());
        } catch (HttpClientErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND) && !e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                // not an acceptable error, re-throw! (hot potato! hot potato!)
                throw e;
            }
        }

        return result;
    }
}
