package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.out.OutBranchDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexmegremis on 07/10/2015.
 */
@Component("branchHistoryEnrichmentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class BranchHistoryEnrichmentProviderImpl extends BaseEnrichmentProviderImpl<OutBranchDTO> {

    List<OutCommitDTO> commits;
    @Value("#{PropertySplitter.map('${branches.protected.prioritised}')}")
    private Map<Integer, String> prioritisedBranches;
    @Value("${enrichment.gitlab.branch.history}")
    private boolean doEnrich;
    @Autowired
    private ParentAwareBaseProvider<OutCommitDTO> uiCommitProvider;

    private void initForProject(final String projectID) {
        if(commits == null) {
            commits = uiCommitProvider.getAll(projectID);
        }
    }

    @Override
    public void enrichAll(final List<OutBranchDTO> dtoList, final String projectID) {
        if(doEnrich) {

            initForProject(projectID);

            Map<String, OutBranchDTO> branchesByName = new HashMap<>();
            // @TODO - rename this one
            Map<OutBranchDTO, String> runner = new HashMap<>();

            for(OutBranchDTO aBranch : dtoList) {
                if (ArrayUtils.isNotEmpty(aBranch.getOldestParentIDs())) {
                    runner.put(aBranch, aBranch.getOldestParentIDs()[0]);
                    branchesByName.put(aBranch.getName(), aBranch);
                }
            }

            for(OutCommitDTO aCommit : commits) {

//                List<OutBranchDTO> matchingBranches = new ArrayList<>();
//                for(OutBranchDTO aBranch : runner.keySet()) {
//                    if(runner.get(aBranch).equals(aCommit.getId())) {
//                        matchingBranches.add(aBranch);
//                    }
//                }

                assignToBranch(aCommit, branchesByName);
            }

//            String firstParent = aDTO.getParentIDs()[0];
//
//            do {
//                OutCommitDTO previous = uiCommitProvider.getSingle(projectID, firstParent);
//                aDTO.getHistory().add(previous);
//
//                firstParent = ArrayUtils.isNotEmpty(previous.getParentIDs()) ? previous.getParentIDs()[0] : null;
//
//            } while (firstParent != null && !firstParent.equals(""));
//
//            aDTO.setEnriched(true);

            System.out.println(">> done with branches");
        }
    }

    private void assignToBranch(final OutCommitDTO commit, final Map<String, OutBranchDTO> branchesByName) {

        System.out.println(">>>> Looking at " + commit.getId());

        OutBranchDTO aBranch = null;

        for (String aPriorityBranchName : prioritisedBranches.values()) {

            if ((aBranch = branchesByName.get(aPriorityBranchName)) != null) {

                if (ArrayUtils.isNotEmpty(aBranch.getOldestParentIDs())) {

                    if (aBranch.getId().equals(commit.getId()) ||
                            aBranch.getOldestParentIDs()[0].equals(commit.getId())) {
                        aBranch.addParent(commit);
                        commit.setBranch(aBranch.getName());
                        System.out.println(">>>>> ATTACHED TO " + aBranch.getName());
                        break;
//                    } else if() {
//                        aBranch.addParent(commit);
//                        commit.setBranch(aBranch.getName());
//                        System.out.println(">>>>> ATTACHED");
//                        break;
                    } else {
                        System.out.println(">>>>> NO MATCH?");
                    }
                } else {
                    System.out.println(">>>> NO PARENTS");
                }
            }
        }
//        for(OutBranchDTO aBranch : branches) {
//
//        }
    }
}
