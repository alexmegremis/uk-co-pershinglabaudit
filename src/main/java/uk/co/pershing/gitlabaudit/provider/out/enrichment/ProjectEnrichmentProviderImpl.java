package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.out.OutBranchDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutProjectDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import java.util.List;

/**
 * Created by alexmegremis on 07/10/2015.
 */
@Component("projectEnrichmentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class ProjectEnrichmentProviderImpl extends BaseEnrichmentProviderImpl<OutProjectDTO> {

    @Autowired
    private ParentAwareBaseProvider<OutBranchDTO> outBranchProvider;

    @Override
    public void enrich(final OutProjectDTO aDTO, final List<OutProjectDTO> projects) {
        List<OutBranchDTO> branches = outBranchProvider.getAll(aDTO.getId());
        aDTO.getBranches().addAll(branches);
        aDTO.setEnriched(true);
    }
}
