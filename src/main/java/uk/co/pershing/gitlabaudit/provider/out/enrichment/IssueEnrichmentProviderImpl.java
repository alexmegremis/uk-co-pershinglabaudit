package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.out.OutIssueCommentDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutIssueDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import java.util.List;

/**
 * Created by alexmegremis on 14/10/2015.
 */
@Component("issueEnrichmentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class IssueEnrichmentProviderImpl extends BaseEnrichmentProviderImpl<OutIssueDTO> {

    @Value("${enrichment.jira.comments}")
    private boolean doEnrich;

    @Autowired
    private ParentAwareBaseProvider<OutIssueCommentDTO> outIssueCommentProvider;

    @Override
    public void enrich(final OutIssueDTO aDTO, final List<OutIssueDTO> list) {
        if (doEnrich && aDTO.getExists() && StringUtils.isNotEmpty(aDTO.getKey())) {
            List<OutIssueCommentDTO> comments = outIssueCommentProvider.getAll(aDTO.getKey());
            aDTO.setComments(comments);
        }
    }
}
