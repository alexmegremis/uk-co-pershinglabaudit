package uk.co.pershing.gitlabaudit.provider.in.gitlab;

import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLProjectDTO;
import uk.co.pershing.gitlabaudit.provider.in.BaseProviderImpl;
import uk.co.pershing.gitlabaudit.util.ParentAwareURIFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("glProjectProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class GLProjectProviderImpl extends BaseProviderImpl<GLProjectDTO> {

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.path.service.projects}")
    private String methodPath;

    @Autowired
    private ParentAwareURIFactory gitLabURIFactory;

    @Override
    protected ParentAwareURIFactory getParentAwareURIFactory() {
        return gitLabURIFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<GLProjectDTO> getType() {
        return GLProjectDTO.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final List<GLProjectDTO> getAll() {
        String url = getParentAwareURIFactory().getUrl(null, getMethodPath(), null);

        GLProjectDTO[] results = getTemplate().getForObject(url, GLProjectDTO[].class);
        List<GLProjectDTO> result = Arrays.asList(results);

        return result;
    }
}
