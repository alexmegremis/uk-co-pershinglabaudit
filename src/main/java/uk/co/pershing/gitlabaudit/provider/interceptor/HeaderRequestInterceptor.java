package uk.co.pershing.gitlabaudit.provider.interceptor;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import java.io.IOException;

/**
 * Add headers to our requests
 * <p/>
 * Created by alexmegremis on 12/11/2015.
 */
public class HeaderRequestInterceptor implements ClientHttpRequestInterceptor {

    private final String headerName;
    private final String headerValue;

    public HeaderRequestInterceptor(final String headerName, final String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    @Override
    public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution) throws IOException {
        HttpRequest wrappedRequest = new HttpRequestWrapper(request);
        wrappedRequest.getHeaders().add(headerName, headerValue);
        ClientHttpResponse result = execution.execute(wrappedRequest, body);
        return result;
    }
}
