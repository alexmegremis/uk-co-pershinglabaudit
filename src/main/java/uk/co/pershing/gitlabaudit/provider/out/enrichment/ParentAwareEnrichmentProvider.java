package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import uk.co.pershing.gitlabaudit.dto.out.OutBaseDTO;

import java.util.List;

/**
 * Created by alexmegremis on 05/10/2015.
 */
public interface ParentAwareEnrichmentProvider<T extends OutBaseDTO> {
    void enrich(final T dto, final String parentID);
    void enrich(final T dto, final List<T> dtoList, final String parentID);
    void enrich(final List<T> dtoList, final String parentID);
}
