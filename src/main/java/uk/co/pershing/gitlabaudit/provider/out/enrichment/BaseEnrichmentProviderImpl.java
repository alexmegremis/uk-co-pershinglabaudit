package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.pershing.gitlabaudit.dto.out.OutBaseDTO;

import java.util.List;

/**
 * Provide placeholder implementations.
 * <p/>
 * Created by alexmegremis on 05/10/2015.
 */
public abstract class BaseEnrichmentProviderImpl<T extends OutBaseDTO> implements EnrichmentProvider<T>, ParentAwareEnrichmentProvider<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseEnrichmentProviderImpl.class);

    /**
     * {@inheritDoc}
     *
     * @param dto
     */
    @Override
    public void enrich(final T dto) {
        throw new NotImplementedException(">>>> Default " + this.getClass().getCanonicalName() + " : enrich(Item) was called. You should override this!");
    }

    /**
     * {@inheritDoc}
     *
     * @param dto
     * @param projectID
     */
    @Override
    public void enrich(final T dto, final String projectID) {
        throw new NotImplementedException(">>>> Default " + this.getClass().getCanonicalName() + " : enrich(Item, Project) was called. You should override this!");
    }

    /**
     * {@inheritDoc}
     *
     * @param dto
     * @param dtoList
     */
    @Override
    public void enrich(final T dto, final List<T> dtoList) {
        throw new NotImplementedException(">>>> Default " + this.getClass().getCanonicalName() + " : enrich(Item, List) was called. You should override this!");
    }

    /**
     * {@inheritDoc}
     *
     * @param dto
     * @param dtoList
     * @param projectID
     */
    @Override
    public void enrich(final T dto, final List<T> dtoList, final String projectID) {
        throw new NotImplementedException(">>>> Default " + this.getClass().getCanonicalName() + " : enrich(Item, List, Project) was called. You should override this!");
    }

    /**
     * {@inheritDoc}
     *
     * @param dtoList
     */
    @Override
    public void enrich(final List<T> dtoList) {
        try {
            for (T aDTO : dtoList) {
                enrich(aDTO, dtoList);
            }
        } catch (NotImplementedException e) {
            enrichAll(dtoList);
        }
    }

    protected void enrichAll(final List<T> dtoList) {
        throw new NotImplementedException(">>>> Default " + this.getClass().getCanonicalName() + " : enrichAll(Item, List, Project) was called. You should override this!");
    }

    /**
     * {@inheritDoc}
     *
     * @param dtoList
     * @param projectID
     */
    @Override
    public void enrich(final List<T> dtoList, final String projectID) {
        try {
            for (T aDTO : dtoList) {
                enrich(aDTO, dtoList, projectID);
            }
        } catch (NotImplementedException e) {
            enrichAll(dtoList, projectID);
        }
    }

    protected void enrichAll(final List<T> dtoList, final String projectID) {
        throw new NotImplementedException(">>>> Default " + this.getClass().getCanonicalName() + " : enrichAll(Item, List, Project) was called. You should override this!");
    }
}
