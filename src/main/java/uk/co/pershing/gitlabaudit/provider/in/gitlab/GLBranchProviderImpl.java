package uk.co.pershing.gitlabaudit.provider.in.gitlab;

import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLBranchDTO;
import uk.co.pershing.gitlabaudit.provider.in.BaseProviderImpl;
import uk.co.pershing.gitlabaudit.util.ParentAwareURIFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("glBranchProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class GLBranchProviderImpl extends BaseProviderImpl<GLBranchDTO> {

    @Autowired
    protected ParentAwareURIFactory gitLabURIFactory;
    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.path.service.branches}")
    private String methodPath;

    @Override
    protected ParentAwareURIFactory getParentAwareURIFactory() {
        return gitLabURIFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<GLBranchDTO> getType() {
        return GLBranchDTO.class;
    }

    /**
     * {@inheritDoc}
     *
     * @return The retrieved items (zero or more, never <b>null</b>)
     */
    @Override
    public final List<GLBranchDTO> getAll(final String projectId) {
        String url = getParentAwareURIFactory().getUrl(projectId, getMethodPath(), null);

        GLBranchDTO[] results = getTemplate().getForObject(url, GLBranchDTO[].class);
        List<GLBranchDTO> result = Arrays.asList(results);

        return result;
    }

}
