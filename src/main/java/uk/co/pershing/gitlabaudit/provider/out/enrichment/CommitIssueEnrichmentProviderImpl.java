package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutIssueDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;
import uk.co.pershing.gitlabaudit.util.MatchFinder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This enricher interrogates Jira for identified issue IDs. The discovered issues
 * are then added to the individual commit.
 * <p/>
 * Created by alexmegremis on 07/10/2015.
 */
@Component("commitIssueEnrichmentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class CommitIssueEnrichmentProviderImpl extends BaseEnrichmentProviderImpl<OutCommitDTO> {

    @Value("${enrichment.jira.issues}")
    private boolean doEnrich;

    @Autowired
    private BaseProvider<OutIssueDTO> outIssueProvider;

    @Autowired
    private MatchFinder<OutCommitDTO> jiraIssueIDFinder;

    @Override
    public void enrich(final OutCommitDTO aDTO, final List<OutCommitDTO> list, final String projectID) {

        if(doEnrich) {

            String[] issueIDs = jiraIssueIDFinder.getAllMatches(aDTO);

            if (ArrayUtils.isNotEmpty(issueIDs)) {
                Map<String, OutIssueDTO> issues = new HashMap<>();
                for (String aIssueID : issueIDs) {
                    OutIssueDTO aIssue = outIssueProvider.getSingle(aIssueID);
                    issues.put(aIssueID, aIssue);
                }
                aDTO.setIssues(issues);
            }
        }
    }
}
