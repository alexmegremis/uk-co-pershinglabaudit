package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import java.util.List;

/**
 * This enricher will populate a commit with parent commits.
 * <p/>
 * Created by alexmegremis on 07/10/2015.
 */
@Deprecated
@Component("commitEnrichmentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class CommitEnrichmentProviderImpl extends BaseEnrichmentProviderImpl<OutCommitDTO> {

    @Value("${enrichment.gitlab.commit}")
    private boolean doEnrich;

    @Autowired
    private ParentAwareBaseProvider<OutCommitDTO> outCommitProvider;

    /**
     * {@inheritDoc}
     *
     * @param aDTO
     * @param projectID
     */
    @Override
    public void enrich(final OutCommitDTO aDTO, final List<OutCommitDTO> list, final String projectID) {

        if(doEnrich) {

            if (ArrayUtils.isEmpty(aDTO.getParentIDs())) {
//                enrich(aDTO, projectID);
//                aDTO.setParentIDs(outCommitProvider.getSingle(projectID, aDTO.getId()).getParentIDs());
            }

            String[] parentIDs = aDTO.getParentIDs();

//        if(parentIDs.length > 1) {
//            CommitDTO[] parents = new CommitDTO[parentIDs.length];
            OutCommitDTO[] parents = new OutCommitDTO[1];

//            for (int i = 0; i < parentIDs.length; i++) {
//                CommitDTO aParentCommit = getSingle(projectID, parentIDs[i]);
//                parents[i] = aParentCommit;
//            }

            if (ArrayUtils.isNotEmpty(parentIDs)) {
                OutCommitDTO aParentCommit = outCommitProvider.getSingle(projectID, parentIDs[0]);
                parents[0] = aParentCommit;
            }
//            aDTO.setParents(parents);

            aDTO.setEnriched(true);
//        }
        }
    }

//    @Override
//    public void enrich (final OutCommitDTO aDTO, final String projectID) {
//        if(doEnrich) {
//            if(ArrayUtils.isEmpty(aDTO.getParentIDs())) {
//                aDTO.setParentIDs(outCommitProvider.getSingle(projectID, aDTO.getId()).getParentIDs());
//            }
//        }
//    }
}
