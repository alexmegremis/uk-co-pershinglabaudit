package uk.co.pershing.gitlabaudit.provider.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLProjectDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutProjectDTO;
import uk.co.pershing.gitlabaudit.provider.BaseProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.EnrichmentProvider;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("outProjectProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ProjectProviderImpl extends BaseExternalDataProviderImpl<OutProjectDTO, GLProjectDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectProviderImpl.class);

    @Autowired
    private EnrichmentProvider<OutProjectDTO> projectEnrichmentProvider;
    @Autowired
    private BaseProvider<GLProjectDTO> glProjectProvider;

    @Override
    protected Class<OutProjectDTO> getType() {
        return OutProjectDTO.class;
    }

    @Override
    protected BaseProvider<GLProjectDTO> getExternalProvider() {
        return glProjectProvider;
    }

    @Override
    public EnrichmentProvider<OutProjectDTO>[] getEnrichmentProviders() {
        return new EnrichmentProvider[]{projectEnrichmentProvider};
    }

}
