package uk.co.pershing.gitlabaudit.provider.in.jira;

import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueDTO;
import uk.co.pershing.gitlabaudit.util.ParentAwareURIFactory;

/**
 * Created by alexmegremis on 13/10/2015.
 */
@Component("jiraIssueProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JiraIssueProviderImpl extends BaseJiraAuthenticatedProvider<JiraIssueDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JiraIssueProviderImpl.class);
    @Autowired
    protected ParentAwareURIFactory jiraURIFactory;
    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.path.service.issues}")
    private String methodPath;

    @Override
    protected ParentAwareURIFactory getParentAwareURIFactory() {
        return jiraURIFactory;
    }

    @Override
    protected Class<JiraIssueDTO> getType() {
        return JiraIssueDTO.class;
    }

    @Override
    public JiraIssueDTO getSingle(final String id) {
        String url = jiraURIFactory.getUrl(null, getMethodPath(), id);

        RestTemplate template = getTemplate();

        ResponseEntity<JiraIssueDTO> response = null;
        HttpStatus status = null;
        JiraIssueDTO result = new JiraIssueDTO();

        try {
            response = template.getForEntity(url, JiraIssueDTO.class);
//            status = response.getStatusCode();
            result = response.getBody();
            result.setExists(true);
        } catch (HttpClientErrorException e) {
            status = e.getStatusCode();
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND) && !e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                // not an acceptable error, re-throw! (hot potato! hot potato!)
                throw e;
            } else {
                result.setExists(false);
            }
        }
//        finally {
//            if(result != null) {
//                result.setExists(!status.equals(HttpStatus.NOT_FOUND));
//            }
//        }

        return result;
    }
}
