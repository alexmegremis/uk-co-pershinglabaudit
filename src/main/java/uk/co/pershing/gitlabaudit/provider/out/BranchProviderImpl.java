package uk.co.pershing.gitlabaudit.provider.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLBranchDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutBranchDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;
import uk.co.pershing.gitlabaudit.provider.out.enrichment.ParentAwareEnrichmentProvider;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("outBranchProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BranchProviderImpl extends BaseExternalDataProviderImpl<OutBranchDTO, GLBranchDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BranchProviderImpl.class);

    //    @Getter
    @Autowired
    private ParentAwareEnrichmentProvider<OutBranchDTO> branchHistoryEnrichmentProvider;
    @Autowired
    private ParentAwareBaseProvider<GLBranchDTO> glBranchProvider;

    @Override
    protected Class<OutBranchDTO> getType() {
        return OutBranchDTO.class;
    }

    @Override
    protected ParentAwareBaseProvider<GLBranchDTO> getExternalParentAwareProvider() {
        return glBranchProvider;
    }

    @Override
    public ParentAwareEnrichmentProvider<OutBranchDTO>[] getParentAwareEnrichmentProviders() {
        return new ParentAwareEnrichmentProvider[]{branchHistoryEnrichmentProvider};
    }
}
