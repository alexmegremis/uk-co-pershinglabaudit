package uk.co.pershing.gitlabaudit.provider.out.enrichment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import uk.co.pershing.gitlabaudit.dto.out.OutBranchDTO;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;
import uk.co.pershing.gitlabaudit.provider.ParentAwareBaseProvider;

import java.util.List;

/**
 * Created by alexmegremis on 07/10/2015.
 */
@Deprecated
@Component("branchEnrichmentProvider")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
public class BranchEnrichmentProviderImpl extends BaseEnrichmentProviderImpl<OutBranchDTO> {

    //    @Value("#{T(java.util.Arrays).asList('${branches.protected}')}")
    private List<String> branchesOfInterest;

    @Value("${enrichment.gitlab.branch}")
    private boolean doEnrich;

    @Autowired
    private ParentAwareBaseProvider<OutCommitDTO> uiCommitProvider;

    public void enrich(final OutBranchDTO aDTO, final String projectID) {
        if(doEnrich && branchesOfInterest.contains(aDTO.getName())) {

//            String[] parentIDs = aDTO.getParentIDs();
//            OutCommitDTO[] parents = new OutCommitDTO[parentIDs.length];
//            for (int i = 0; i < parentIDs.length; i++) {
//
//                OutCommitDTO aParentCommit = uiCommitProvider.getSingle(projectID, parentIDs[i]);
//                parents[i] = aParentCommit;
//            }
//
//            aDTO.setParents(parents);
//
//            aDTO.setEnriched(true);
        }
    }
}
