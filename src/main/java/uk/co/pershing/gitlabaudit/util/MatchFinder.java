package uk.co.pershing.gitlabaudit.util;

/**
 * Look for patterns. (spooky?)
 * <p/>
 * Created by alexmegremis on 15/10/2015.
 */
public interface MatchFinder<T> {

    /**
     * Return all matching sections of text.
     *
     * @param input
     * @return
     */
    String[] getAllMatches(final T input);
}
