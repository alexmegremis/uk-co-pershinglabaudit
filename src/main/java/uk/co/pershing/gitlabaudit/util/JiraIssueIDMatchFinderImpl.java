package uk.co.pershing.gitlabaudit.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import uk.co.pershing.gitlabaudit.dto.out.OutCommitDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a pattern matcher that specifically looks for jira issue IDs in commit messages.
 * <p/>
 * Created by alexmegremis on 15/10/2015.
 */
@Component("jiraIssueIDFinder")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class JiraIssueIDMatchFinderImpl implements MatchFinder<OutCommitDTO> {

    private static Pattern compiledPattern = null;
    @Value("${constant.regex.jiraissue}")
    private String jiraPattern;

    private Pattern getCompiledPattern() {
        if (compiledPattern == null) {
            synchronized (this) {
                if (compiledPattern == null) {
                    compiledPattern = Pattern.compile(jiraPattern);
                }
            }
        }

        return compiledPattern;
    }

    /**
     * {@inheritDoc}
     *
     * @param aCommit
     * @return
     */
    @Override
    public String[] getAllMatches(final OutCommitDTO aCommit) {

        Matcher m = getCompiledPattern().matcher(aCommit.getMessage());

        List<String> matches = new ArrayList<>();

        while (m.find()) {
            String candidate = m.group();
            if (matches.contains(candidate)) {
                matches.add(candidate);
            }
        }
        String[] result = matches.toArray(new String[matches.size()]);

        return result;
    }
}
