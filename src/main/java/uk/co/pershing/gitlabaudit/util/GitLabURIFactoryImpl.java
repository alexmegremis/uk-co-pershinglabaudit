package uk.co.pershing.gitlabaudit.util;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("gitLabURIFactory")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GitLabURIFactoryImpl extends BaseURIFactoryImpl implements ParentAwareURIFactory {

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.hostname}")
    private String hostname;

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.protocol}")
    private String scheme;

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.port}")
    private int port;

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.path.context}")
    private String context;

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.path.api.root}")
    private String apiRoot;

    @Getter(AccessLevel.PROTECTED)
    @Value("${gitlab.path.api.version}")
    private String apiVersion;

    // TODO: This might be a bit of a cheat. Perhaps we should be getting this out of the appropriate provider class.
    @Value("${gitlab.path.service.projects}")
    private String projectsPath;


    @Value("${gitlab.param.key.pagesize}")
    private String urlParamKeyPageSize;
    @Value("${gitlab.param.value.pagesize}")
    private String urlParamValuePageSize;

    @Value("${gitlab.param.key.usertoken}")
    private String urlParamKeyToken;
    @Value("${gitlab.param.value.usertoken}")
    private String urlParamValueToken;

    @Getter
    @Setter
    @Value("${gitlab.param.key.pagenum}")
    private String urlParamKeyPageNum;


    /**
     * {@InheritDoc}
     */
    @Override
    public String getUrl(final String projectId, final String methodPath, final String id) {

        UriComponentsBuilder builder = getUrlBuilderForAPI(projectsPath, projectId, methodPath, id);

        builder.queryParam(urlParamKeyToken, urlParamValueToken);
        builder.queryParam(urlParamKeyPageSize, urlParamValuePageSize);
        // @TODO - remove this hardcoded pagenum
        builder.queryParam(urlParamKeyPageNum, 0);

        String result = getUrl(builder);
        return result;
    }
}
