package uk.co.pershing.gitlabaudit.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Parent class to do away with API-specific details.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public abstract class BaseURIFactoryImpl implements ParentAwareURIFactory {

    protected abstract String getHostname();

    protected abstract int getPort();

    protected abstract String getScheme();

    protected abstract String getContext();

    protected abstract String getApiRoot();

    protected abstract String getApiVersion();

    /**
     * Create a API-specific URI builder for the child class to add param details to.
     *
     * @return
     */
    protected final UriComponentsBuilder getUrlBuilderForAPI(final String parentPath, final String parentId, final String methodPath, final String id) {

        UriComponentsBuilder result = UriComponentsBuilder.newInstance()
                .host(getHostname())
                .port(getPort())
                .scheme(getScheme());

        result.pathSegment(getContext(), getApiRoot(), getApiVersion());

        if (StringUtils.isNotEmpty(parentId)) {
            result.pathSegment(parentPath, parentId);
        }
        if (StringUtils.isNotEmpty(methodPath)) {
            result.pathSegment(methodPath);
        }
        if (StringUtils.isNotEmpty(id)) {
            result.pathSegment(id);
        }

        return result;
    }

    /**
     * Once parameterisation of the builder is complete by a subclass, produce a URL as text.
     *
     * @param builder
     * @return
     */
    protected final String getUrl(final UriComponentsBuilder builder) {

        UriComponents uriComponents = builder.build();

        String result = uriComponents.toUriString();

        return result;
    }
}
