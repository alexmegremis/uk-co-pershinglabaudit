package uk.co.pershing.gitlabaudit.util;

/**
 * Created by alexmegremis on 29/09/15.
 */
public interface ParentAwareURIFactory {
    String getUrl(final String projectId, final String methodPath, final String id);
}
