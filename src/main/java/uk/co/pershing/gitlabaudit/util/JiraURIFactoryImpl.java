package uk.co.pershing.gitlabaudit.util;

import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Component("jiraURIFactory")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JiraURIFactoryImpl extends BaseURIFactoryImpl implements ParentAwareURIFactory {

    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.hostname}")
    private String hostname;

    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.protocol}")
    private String scheme;

    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.port}")
    private int port;

    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.path.context}")
    private String context;

    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.path.api.root}")
    private String apiRoot;

    @Getter(AccessLevel.PROTECTED)
    @Value("${jira.path.api.version}")
    private String apiVersion;

    @Value("${jira.path.service.issues}")
    private String issuesPath;


    /**
     * {@InheritDoc}
     */
    @Override
    public String getUrl(final String issueId, final String methodPath, final String id) {

        UriComponentsBuilder builder = getUrlBuilderForAPI(issuesPath, issueId, methodPath, id);

        String result = getUrl(builder);
        return result;
    }

}
