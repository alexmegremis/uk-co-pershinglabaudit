package uk.co.pershing.gitlabaudit.dto.out;

import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueCommentDTO;

/**
 *
 * Created by alexmegremis on 13/11/2015.
 */
@Data
public class OutIssueCommentDTO extends OutBaseDTO<OutIssueCommentDTO, JiraIssueCommentDTO> {

    private String foo;
    private String authorName;
    private String authorEmail;
    private String id;
    private String body;

    @Override
    public void init(final JiraIssueCommentDTO dto) {
        this.authorName = dto.getAuthor().getDisplayName();
        this.authorEmail = dto.getAuthor().getEmailAddress();
        this.id = dto.getId();
        this.body = dto.getBody();
    }
}
