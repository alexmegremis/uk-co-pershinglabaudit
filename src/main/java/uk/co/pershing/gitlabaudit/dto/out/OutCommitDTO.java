package uk.co.pershing.gitlabaudit.dto.out;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLCommitDTO;

import java.util.Map;

/**
 * Created by alexmegremis on 29/09/15.
 */
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
@JsonIgnoreProperties(ignoreUnknown = false)
public class OutCommitDTO extends OutBaseDTO<OutCommitDTO, GLCommitDTO> {

//    // @TODO - do we want to keep this? Commits's relationships don't change, but messages can.
//    private static final Map<String, OutCommitDTO> commits = new HashMap<>();

    @Getter
    private String authorName;
    @Getter
    private String authorEmail;
    @Getter
    private String dateTimeCommitted;
    @Getter
    private String message;
    @Getter
    private String id;
    @Getter
    @Setter
//    @JsonIgnore
    private String[] parentIDs;
    //    @Getter
//    @Setter
//    private OutCommitDTO[] parents;
    @Getter
    @Setter
    private String branch;
    @Getter
    @Setter
    private Map<String, OutIssueDTO> issues;

    public boolean isMerge() {
        return parentIDs != null && parentIDs.length > 1;
    }

    @Override
    public void init(final GLCommitDTO glCommitDTO) {
        this.authorName = glCommitDTO.getAuthorName();
        this.authorEmail = glCommitDTO.getAuthorEmail();
        this.dateTimeCommitted = glCommitDTO.getDateTimeCommitted();
        this.id = glCommitDTO.getId();
        this.message = glCommitDTO.getMessage();
        this.parentIDs = glCommitDTO.getParentIds();
    }
}
