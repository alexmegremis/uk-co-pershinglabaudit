package uk.co.pershing.gitlabaudit.dto.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLProjectDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class OutProjectDTO extends OutBaseDTO<OutProjectDTO, GLProjectDTO> {

    @Getter
    private final List<OutBranchDTO> branches = new ArrayList<>();
    @Getter
    private String id;
    @Getter
    private String name;

    @Override
    public void init(final GLProjectDTO glDTO) {
        this.id = glDTO.getId();
        this.name = glDTO.getName();
    }
}
