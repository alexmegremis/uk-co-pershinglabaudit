package uk.co.pershing.gitlabaudit.dto.out;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;

/**
 * Created by alexmegremis on 01/10/2015.
 */

public abstract class OutBaseDTO<T extends OutBaseDTO, Y extends Object> extends BaseDTO {

    @Getter
    @Setter
    @JsonIgnore
    private boolean enriched = false;

    public abstract void init(final Y glDTO);
}
