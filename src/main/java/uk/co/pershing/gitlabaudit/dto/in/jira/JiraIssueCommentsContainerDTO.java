package uk.co.pershing.gitlabaudit.dto.in.jira;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;

import java.util.Collection;

/**
 * Created by alexmegremis on 13/11/2015.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraIssueCommentsContainerDTO extends BaseDTO {
    private String id;
    private Collection<JiraIssueCommentDTO> comments;
}
