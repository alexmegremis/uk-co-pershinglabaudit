package uk.co.pershing.gitlabaudit.dto.in.jira;

import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraIssueCommentDTO extends BaseDTO {

    private String id;
    private String body;
    private AuthorDTO author;

    // We hate inner classes, but we need to work with the type provided to us by the
    // Jira API without writing throwaway types.
    @Data
    public class AuthorDTO {
        //        @JsonProperty("display_name")
        private String displayName;
        //        @JsonProperty("email_address")
        private String emailAddress;
    }
}
