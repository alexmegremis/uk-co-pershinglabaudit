package uk.co.pershing.gitlabaudit.dto.in.gl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;

/**
 * Created by alexmegremis on 01/10/2015.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = false)
public class GLBranchDTO extends BaseDTO {

    private String name;
    @JsonProperty("protected")
    private Boolean protectedBranch;
    @JsonProperty("commit")
    private GLCommitDTO lastCommit;

    @Override
    public String getId() {
        return this.getName();
    }
}
