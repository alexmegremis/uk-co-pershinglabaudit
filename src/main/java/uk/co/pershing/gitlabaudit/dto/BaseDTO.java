package uk.co.pershing.gitlabaudit.dto;

import java.io.Serializable;

/**
 * Created by alexmegremis on 01/10/2015.
 */
public abstract class BaseDTO implements Serializable {
    public abstract String getId();
}
