package uk.co.pershing.gitlabaudit.dto.in.jira;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraIssueDTO extends BaseDTO {
    private String id;
    private String key;
    private boolean exists;
}
