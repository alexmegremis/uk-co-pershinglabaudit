package uk.co.pershing.gitlabaudit.dto.in.gl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.BaseDTO;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = false)
public class GLProjectDTO extends BaseDTO {
    private String name;
    private String id;
    private String nameWithNamespace;
}
