package uk.co.pershing.gitlabaudit.dto.out;

import lombok.Getter;
import lombok.Setter;
import uk.co.pershing.gitlabaudit.dto.in.gl.GLBranchDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
public class OutBranchDTO extends OutBaseDTO<OutBranchDTO, GLBranchDTO> {

    @Getter
    private String name;
    @Getter
    private Boolean protectedBranch;
    //    @JsonIgnore
    @Getter
    private String[] oldestParentIDs;
//    @Getter
//    @Setter
//    private OutCommitDTO[] parents;

    @Getter
    private List<OutCommitDTO> history = new ArrayList<>();

    @Getter
    @Setter
    private String id;

    public void addParent(final OutCommitDTO parent) {
        history.add(parent);
//        this.id = parent.getId();
        this.oldestParentIDs = parent.getParentIDs();
    }
//    public String getNextId() {
//        return parentIDs[0];
//    }

    @Override
    public void init(final GLBranchDTO glBranchDTO) {
        this.name = glBranchDTO.getName();
        this.protectedBranch = glBranchDTO.getProtectedBranch();
        OutCommitDTO head = new OutCommitDTO();
        head.init(glBranchDTO.getLastCommit());
        this.addParent(head);
//        this.oldestParentIDs = glBranchDTO.getLastCommit().getParentIds();
        this.id = glBranchDTO.getLastCommit().getId();
    }
}
