package uk.co.pershing.gitlabaudit.dto.out;

import lombok.Data;
import uk.co.pershing.gitlabaudit.dto.in.jira.JiraIssueDTO;

import java.util.List;

/**
 * Created by alexmegremis on 29/09/15.
 */
@Data
public class OutIssueDTO extends OutBaseDTO<OutIssueDTO, JiraIssueDTO> {

    private String key = "";
    private Boolean exists;
    private List<OutIssueCommentDTO> comments;

    @Override
    public void init(final JiraIssueDTO dto) {
        this.exists = dto.isExists();
        if (dto.isExists()) {
            this.key = dto.getKey();
        }
    }

    @Override
    public String getId() {
        return key;
    }


}
